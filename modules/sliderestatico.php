<div class="BannerEstatico"></div>

<section class="jd-slider fade-slider">
    <div class="slide-inner">
        <ul class="slide-area">
            <li>
                <div class="d-none d-md-block bannerCategoria">
                    <h1 class="enunciadoBanner"> Banner Categoria</h1>
                    <h5>Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam debitis sed eligendi</h5>
                </div>
                <img src="<?php echo get_template_directory_uri();?>/img/bannerGrande01.jpg"" alt="img1" class="img-fluid">
            </li>
        </ul>
    </div>
    <div class="controller d-none">
        <a class="auto" href="#">
            <i class="fas fa-play fa-xs"></i>
            <i class="fas fa-pause fa-xs"></i>
        </a>
        <div class="indicate-area"></div>
    </div>
</section>