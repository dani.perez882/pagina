<?php 

function estilos() {
   
 wp_enqueue_style('bootstrapp','https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', false,'1.1','all');
 wp_enqueue_style('googleFonts','https://fonts.googleapis.com/css?family=Chewy|Open+Sans:300,400', false,'1.1','all');
 wp_enqueue_style('fontsawasome','https://use.fontawesome.com/releases/v5.6.3/css/all.css',false,'1.1','all');
 wp_enqueue_style('style',get_template_directory_uri().'/css/style.css', false,'1.1','all');
 wp_enqueue_style('stylejdSlider',get_template_directory_uri().'/css/jquery.jdSlider.css', false,'1.1','all');

 wp_enqueue_script('jquery','https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(),1.1,false);
 wp_enqueue_script('popper','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array(),1.1,false);
 wp_enqueue_script('javascriptbootstrapp','https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', array(),1.1,false);
 wp_enqueue_script('TweenMax','https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js', array(),1.1,true);

  /**archivos locales */
 
 wp_enqueue_script('jdSlider',get_template_directory_uri().'/js/jquery.jdSlider-latest.js', array(),1.1,false);
 wp_enqueue_script('scrollUP',get_template_directory_uri().'/js/scrollUP.js', array(),1.1,false);
 wp_enqueue_script('easing',get_template_directory_uri().'/js/jquery.easing.js', array(),1.1,false);
 wp_enqueue_script('pagination',get_template_directory_uri().'/js/pagination.min.js', array(),1.1,false);
 wp_enqueue_script('superscrollorama',get_template_directory_uri().'/js/jquery.superscrollorama.js', array(),1.1,false);
 wp_enqueue_script('script',get_template_directory_uri().'/js/script.js', array(),1.1,true);
}

add_action('wp_enqueue_scripts', 'estilos');


function theme_setup() {
/** funcion agregar el menù */

register_nav_menus( array(
    'header-menu'   => __( 'Header Menu', 'viajero' ),
    
) );

add_filter("nav_menu_link_attributes","agregarClases",10,3);
function agregarClases($atts,$item,$args){
    $class= "nav-link text-white";
    $atts["class"]= $class;
    return $atts;
}


add_theme_support("post-thumbnails");

}



add_action( 'after_setup_theme', 'theme_setup' );
?>