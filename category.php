<?php get_header(); ?>
<!-- Slide -->
<?php 
include("modules/sliderestatico.php");
?>
<?php 
include("modules/searchMovil.php");
?>
<!--menu-->
<?php 
include("modules/menu.php");
?>
<!-- contenido Blog -->
<div class="container-fluid pt-5 contenido">
    <div class="container p-0">
    <ul class="breadcrumb mb-3 bg-white">
        <li class="breadcrumb-item"><a href="<?php echo esc_url(home_url("/"));  ?>">Inicio</a></li>
      <?php
      $cat = get_query_var('cat');
      $nombreCategoria = get_category($cat)->name;
      $linkCategoria= get_category_link($cat);
      ?>
      <li class="breadcrumb-item"><a href="#"><?php echo $nombreCategoria;?></a></li>
    </ul>
        <div class="row">
            <div class="col-12 col-lg-9 pr-0 pr-lg-5">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="row">
                    <div class="col-12 col-lg-5">
                        <a href="<?php the_permalink();?>">
                            <h4 class="d-block d-lg-none"><?php the_title(); ?></h4>
                        </a>
                        <a href="<?php the_permalink();?>">
                        <?php the_post_thumbnail("post-thumbnails", array("class"=>"img-fluid")); ?>
                        </a>
                    </div>
                    <div class="col-12 col-lg-7 contenidoArt">
                        <a href="<?php the_permalink();?>">
                            <h4 class="d-none d-lg-block"><?php the_title(); ?></h4>
                        </a>
                        <p class="my-4"> <?php the_excerpt(); ?></p>

                        <a href="<?php the_permalink();?>" class="float-right mr-0 mr-lg-2"> Leer Más</a>
                        <div class="fecha d-none d-lg-block"><?php the_time("d.m.Y") ?></div>
                    </div>
                </div>
                <hr class="mb-3 mb-lg-5" style="border: 1px solid green; width: 100%">

<?php endwhile;
 endif;
  ?>                     
                <div class="container d-none d-md-block">
                    <ul class="paginacion justify-content-center"></ul>
                </div>

                <!--fin articulos  -->
            </div>
<?php get_sidebar(); ?>
</div>
</div>
</div>
<?php get_footer(); ?>