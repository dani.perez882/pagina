<footer class="p-3 bg-white d-block d-md-none"></footer>

<footer class="container-fluid p-5 d-none d-md-block">

    <div class="container p-0">

        <div class="row">
            <div class="col-6 bg-white p-2">
                <div class="container flex-fill p-0 grillaFooter">

                    <div class="d-flex flex-fill p-0">
                        <div class="d-block d-md-flex flex-column flex-fill columna1">
                            <figure class="flex-fill photo1" vinculo="categoria.html">
                                <p>Surámerica</p>
                            </figure>
                            <figure class="photo2">
                                <p>África</p>
                            </figure>
                            <figure class="d-block d-md-none flex-fill photo6" vinculo="categoria.html">
                                <p>Asia</p>
                            </figure>
                        </div>
                        <div class="d-block d-md-flex flex-column flex-fill">
                            <div class="d-block d-md-flex">
                                <figure class="flex-fill photo3" vinculo="categoria.html">
                                    <p>CentroÁmerica</p>
                                </figure>
                                <figure class="flex-fill photo4" vinculo="categoria.html">
                                    <p>Europa</p>
                                </figure>
                            </div>
                            <figure class="flex-fill photo5" vinculo="categoria.html">
                                <p>NorteÁmerica</p>
                            </figure>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-md-1 col-lg-2"></div>
            <div class="col-md-5  col-lg-4 my-5">
                <h6 class="text-white mb-3">Inscríbite a nuestro Newletter</h6>
                <div class="input-group">
                    <input type="text" placeholder="Email" class="form-control">
                    <div class="input-group-append">
                        <span class="input-group-text bg-dark">Inscribirse</span>


                    </div>
                </div>

                <!-- inicio redes -->
                <div class="pt-2 mt-1 ">
                    <ul class="d-flex justify-content-left">
                        <li>
                            <a href="#" target="_blank">
                                <i class="fab fa-facebook-f  text-white mr-3 lead"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class="fab fa-instagram text-white mr-3 lead"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class="fab fa-twitter text-white mr-3 lead"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class="fab fa-youtube text-white mr-3 lead"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class="fab fa-snapchat-ghost text-white mr-3 lead"></i>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>

        </div>



        <!-- fin redes -->

    </div>
    </div>
    <div class="justify-content-center text-center text-white my-3">Diseñado - Curso Udemy Front-end</div>
    </div>

</footer>
<!-- scrollorama -->
<?php wp_footer(); ?>
</body>

</html>