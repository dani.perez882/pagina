<!DOCTYPE html>
<html lang="es">

<head>



    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Blog Viajero | Curso Udemy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- favicon -->
    <link rel="icon" href="<?php echo get_template_directory_uri();?>/img/icono.jpg"" type="image/x-icon">

<?php wp_head();?>

</head>

<body>

    <!-- preload -->

    <div class="preload">

        <div class="porcentajecarga">0%</div>
        <div class="lineacarga">
            <div class="rellenocarga"></div>
        </div>
        <div class="fondocarga"></div>

    </div>


    <!-- Header  -->
    <header class="container-fluid">
        <div class="container p-0">
            <div class="row">
                <div class="col-10 col-sm-11 col-md-7 pt-1 pt-lg-0">
                    <a href="index.html">
                        <img src="<?php echo get_template_directory_uri();?>/img/logotipo-negativo.png" alt="Logo Juanito Travel" class="img-fluid logotipo">
                    </a>
                </div>
                <!-- redes sociales -->
                <div class="col-md-3 d-none d-md-block redes pt-2 mt-1">
                    <ul class="d-flex justify-content-end">
                        <li>
                            <a href="#" target="_blank">
                                <i class="fab fa-facebook-f  text-white mr-1 lead rounded-circle"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class="fab fa-instagram text-white mr-1 lead rounded-circle"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class="fab fa-twitter text-white mr-1 lead rounded-circle"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class="fab fa-youtube text-white mr-1 lead rounded-circle"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class="fab fa-snapchat-ghost text-white mr-1 lead rounded-circle"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- buscador y menu -->
                <div class="col-2 col-sm-1 col-md-2 d-flex justify-content-end pt-2 mt-1">
                    <div class="d-none d-md-block pr-3 pr-lg-5 mt-1">
                        <i class="fa fa-search " aria-hidden="true" data-toggle="collapse" data-target="#buscador"></i>
                    </div>
                    <div class="mt-0 mt-sm-2 mt-md-0 pr-0 pr-md-3 ">
                        <i class="fa fa-bars " aria-hidden="true"></i>
                    </div>
                </div>

                <!-- input buscador  -->
                <div id="buscador" class="collapse col-12 ">
                    <div class="input-group input-group pb-2 col-5 float-right">
                        <input type="text" class="form-control" placeholder="Buscar">
                        <div class="input-group-append">
                            <button class="btn btn-light" type="submit">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </header>
    <!-- redes sociales moviles -->
    <div class="col-md-3 d-block d-md-none redes pt-2 redesMoviles bg-white">
        <ul class="d-flex justify-content-center p-0">
            <li>
                <a href="#" target="_blank">
                    <i class="fab fa-facebook-f  text-white mr-2 mr-sm-4 lead rounded-circle"></i>
                </a>
            </li>
            <li>
                <a href="#" target="_blank">
                    <i class="fab fa-instagram text-white mr-2 mr-sm-4 lead rounded-circle"></i>
                </a>
            </li>
            <li>
                <a href="#" target="_blank">
                    <i class="fab fa-twitter text-white mr-2 mr-sm-4 lead rounded-circle"></i>
                </a>
            </li>
            <li>
                <a href="#" target="_blank">
                    <i class="fab fa-youtube text-white mr-2 mr-sm-4 lead rounded-circle"></i>
                </a>
            </li>
            <li>
                <a href="#" target="_blank">
                    <i class="fab fa-snapchat-ghost text-white mr-2 mr-sm-4 lead rounded-circle"></i>
                </a>
            </li>
        </ul>





    </div>
