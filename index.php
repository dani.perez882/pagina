<?php get_header(); ?>

 <!-- Slide -->
 <div class="BannerEstatico"></div>

<section class="jd-slider fade-slider">
    <div class="slide-inner">
        <ul class="slide-area">
            <li>
                <img src="<?php echo get_template_directory_uri();?>/img/banner01.jpg" alt="img1" class="img-fluid">
            </li>
            <li>
                <img src="<?php echo get_template_directory_uri();?>/img/banner02.jpg" alt="img2" class="img-fluid">
            </li>
            <li>
                <img src="<?php echo get_template_directory_uri();?>/img/banner03.jpg" alt="img3" class="img-fluid">
            </li>
            <li>
                <img src="<?php echo get_template_directory_uri();?>/img/banner04.jpg" alt="img4" class="img-fluid">
            </li>
        </ul>
    </div>
    <div class="controller d-none">
        <a class="auto" href="#">
            <i class="fas fa-play fa-xs"></i>
            <i class="fas fa-pause fa-xs"></i>
        </a>
        <div class="indicate-area"></div>
    </div>
</section>

<!-- Buscador movil-->
<?php 
include("modules/searchMovil.php");
?>



<!--menu-->
<?php 
include("modules/menu.php");
?>


<!-- categorias blog -->
<div class="container-fluid pt-5 grilla p-0">

    <div class="container flex-fill p-0">

        <div class="d-flex flex-fill p-0">
            <div class="d-block d-md-flex flex-column flex-fill columna1">
                <figure class="flex-fill photo1" vinculo="<?php echo get_template_directory_uri();?>/category.php">
                    <p>Mi viaje por Surámerica</p>
                </figure>
                <figure class="photo2">
                    <p>Mi viaje por África</p>
                </figure>
                <figure class="d-block d-md-none flex-fill photo6" vinculo="categoria.html">
                    <p>Mi Viaje por Asia</p>
                </figure>
            </div>
            <div class="d-block d-md-flex flex-column flex-fill">
                <div class="d-block d-md-flex">
                    <figure class="flex-fill photo3" vinculo="category.php">
                        <p>Mi viaje por Centro Ámerica</p>
                    </figure>
                    <figure class="flex-fill photo4" vinculo="category.php">
                        <p>Mi viaje por Europa</p>
                    </figure>

                </div>
                <figure class="flex-fill photo5" vinculo="categoria.html">
                    <p>Mi Viaje por Norte Ámerica</p>
                </figure>

            </div>
        </div>
    </div>


</div>

<!-- contenido Blog -->
<div class="container-fluid pt-5 contenidoBLog">
    <div class="container p-0">
        <div class="row">
            <div class="col-12 col-lg-9 pr-0 pr-lg-5">
                <!-- ARTICULO 1 -->

<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="row">
                    <div class="col-12 col-lg-5">
                        <a href="<?php the_permalink();?>">
                            <h4 class="d-block d-lg-none"><?php the_title(); ?></h4>
                        </a>
                        <a href="<?php the_permalink();?>">
                        <?php the_post_thumbnail("post-thumbnails", array("class"=>"img-fluid")); ?>
                        </a>
                    </div>
                    <div class="col-12 col-lg-7 contenidoArt">
                        <a href="<?php the_permalink();?>">
                            <h4 class="d-none d-lg-block"><?php the_title(); ?></h4>
                        </a>
                        <p class="my-4"> <?php the_excerpt(); ?></p>

                        <a href="<?php the_permalink();?>" class="float-right mr-0 mr-lg-2"> Leer Más</a>
                        <div class="fecha d-none d-lg-block"><?php the_time("d.m.Y") ?></div>
                    </div>
                </div>
                <hr class="mb-3 mb-lg-5" style="border: 1px solid green; width: 100%">
<?php endwhile;
 endif;
  ?>

              

             
                <div class="container d-none d-md-block">
                    <ul class="pagination justify-content-center">
                    <?php echo paginate_links();?>
                    </ul>
                </div>

                <!--fin articulos  -->
            </div>



<?php get_sidebar(); ?>


</div>
</div>
</div>


<?php get_footer(); ?>