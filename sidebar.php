<div class=" d-none d-lg-block col-lg-3 p-0 ">
                    <h4>Sobre Mi</h4>
                    <div class="row">
                        <div class="col-12 pt-3">

                            <img src="<?php echo get_template_directory_uri();?>/img/sobreMi.jpg" alt="sobre mi" class="img-fluid">
                            <p class="small pt-2">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eveniet ex temporibus, impedit perferendis, voluptatum eos minus delectus, commodi esse qui ut aperiam asperiores. Ratione, debitis quibusdam aspernatur magnam qui et.</p>
                        </div>

                        <div class="col-12 ads pt-4">
                            <img src="<?php echo get_template_directory_uri();?>/img/ad01.jpg" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-12 pt-3 pb-3">
                            <h5 class="pt-3">Comentarios recientes</h5>
                        </div>
                        <div class="d-flex">
                            <a href="articulos.html">
                                <img src="<?php echo get_template_directory_uri();?>/img/com01.jpg" alt="" class="mr-2" height="85%">
                            </a>
                            <p class="small">Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic atque maiores vitae doloribus accusantium aut aperiam earum delectus recusandae quo eaque ipsam nobis ab.</p>
                        </div>
                        <div class="d-flex">
                            <a href="articulos.html">
                                <img src="<?php echo get_template_directory_uri();?>/img/com02.jpg" alt="" class="mr-2" height="85%">
                            </a>
                            <p class="small">Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic atque maiores vitae doloribus accusantium aut aperiam earum delectus recusandae quo eaque ipsam nobis ab.</p>
                        </div>
                        <div class="d-flex">
                            <a href="articulos.html">
                                <img src="<?php echo get_template_directory_uri();?>/img/com03.jpg" alt="" class="mr-2" height="85%">
                            </a>
                            <p class="small">Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic atque maiores vitae doloribus accusantium aut aperiam earum delectus recusandae quo eaque ipsam nobis ab.</p>
                        </div>
                    </div>
                    <div class="ads p-0">
                        <img src="<?php echo get_template_directory_uri();?>/img/ad02.jpg" alt="" class="pt-4" width="100%">
                    </div>

                </div>
            </div>

        </div>