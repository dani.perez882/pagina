$ = jQuery.noConflict();
$("a").attr("target", "_parent")

// Banner
$(".fade-slider").jdSlider({

    isSliding: false,
    isAuto: true,
    isLoop: true,
    isDrag: false,
    interval: 5000,
    isCursor: false,
    speed: 3000
});
var AlturaBanner = $(".fade-slider").height();
$(window).scroll(function() {
    var positionY = window.pageYOffset;
    if (positionY > AlturaBanner) {
        $("header").css({ "background-color": "white" });
        $(".logotipo").css({ "filter": "invert(100%)" });
        $(".fa-bars, .fa-search").css({ "color": "black" })
    } else {
        $("header").css({ "background-color": "rgba(0,0,0,.5)" });
        $(".logotipo").css({ "filter": "invert(0%)" });
        $(".fa-bars, .fa-search").css({ "color": "white" })
    }
});
// comportamiento boton menu y cerrar menu
$(".fa-bars").click(function() {
    $(".menu").fadeIn("fast")
});

$(".btnClose").click(function() {
    $(".menu").fadeOut("fast")
});

// animando grilla categorias

$(".grilla figure").mouseover(function() {

    $(this).css({ "background-position": "right bottom" })
})
$(".grilla figure").mouseout(function() {

    $(this).css({ "background-position": "left top" })
})


$(".grilla figure, .grillaFooter figure").click(function() {

    var vinculo = $(this).attr("vinculo");
    window.location = vinculo;


})


// paginacion
// $(".paginacion").twbsPagination({
//     totalPages: 10,
//     visiblePages: 4,
//     first: "Primera",
//     last: "Ultima",
//     prev: '<i class="fas fa-angle-left"></i>',
//     next: '<i class="fas fa-angle-right"></i>'

// });

$(".pagination .page-numbers").before('<li class="page-item">');

var paginatioNumber = $(".pagination .page-numbers");
var pageItem = $(".pagination .page-item")

for (var a = 0; a <= pageItem.length; a++) {
    $(paginatioNumber[a]).appendTo($(pageItem[a]));
    $(paginatioNumber[a]).addClass("page-link");
}

$(".pagination .page-numbers.current").parent().addClass("active");
$(".pagination .page-numbers.next").html('<i class="fas fa-angle-right"></i>');
$(".pagination .page-numbers.prev").html('<i class="fas fa-angle-left"></i>');
// superscrollorama
var controller = $.superscrollorama();


controller.addTween('.contenidoBLog .container',
    TweenMax.from($('.contenidoBLog .container'), .5, { css: { opacity: 0 } }));


// scrollUP

$.scrollUp({
    scrollSpeed: 500,
    easingType: 'easeOutQuint',
    zIndex: 2147483647, // Z-Index for the overlay
    scrollText: ''
})

// preload

$("body").css({ "overflow-y": "hidden" });
var imagenes = $("img");
var scripts = $("script");
var css = $("link");
var figure = $("figure");

var total = [];
var porcentajecarga = 0;
var items = 0;
var archivos = imagenes.length + scripts.length + css.length + figure.length;
archivos = 100 / archivos;

total.push(imagenes, scripts, css, figure);

total.forEach(element => {

    for (let index = 0; index < element.length; index++) {
        items++;
        porcentajecarga = Math.ceil(items * archivos);

        $(".porcentajecarga").html(porcentajecarga + "%");
        $(".rellenocarga").css({ "width": porcentajecarga + "%" });


        if ($(document).ready(function() {}) && porcentajecarga >= 100) {
            $(".preload").delay(370).fadeOut("slow")
            $("body").delay(350).css({ "overflow-y": "scroll" })
        }
    }

});


// Deslizador articulos

$(".sliderArticulos").jdSlider({
    wrap: ".slide-inner",
    slideShow: 3,
    slideToScroll: 3,
    isLoop: true,
    responsive: [{
        viewSize: 360,
        settings: {
            slideShow: 1,
            slideToScroll: 1,
        }

    }]

})