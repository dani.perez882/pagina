<!-- cabecera -->
<?php get_header(); ?>
<!-- Slide -->
<?php 
include("modules/sliderestatico.php");
?>
<!--Buscador movil-->
<?php 
include("modules/searchMovil.php");
?>
<!--menu-->
<?php 
include("modules/menu.php");
?>
<!-- contenido Blog -->
    <div class="container-fluid pt-2 contenido">
        <div class="container p-0">

     <?php
      
      $nombreCategoria = get_the_category();
      $linkCategoria= get_category_link($nombreCategoria[0]->term_id);
      
      ?>
            <ul class="breadcrumb mb-3 bg-white">

           <li class="breadcrumb-item"><a href="<?php echo esc_url(home_url("/"));?>">Inicio</a></li>
           <li class="breadcrumb-item"><a href="<?php echo $linkCategoria;?>"><?php echo $nombreCategoria[0]->name;?></a></li>
             <li class="breadcrumb-item active"><a href="#"><?php the_title();?></a></li>
            </ul>
            <div class="row">
                <div class="col-12 col-lg-9 pr-0 pr-lg-5">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                   <div class="row">
                        <div class="col-12">
                            <div class="d-flex pb-2">
                                <div class="fechaArticulo"><?php the_time("d.m.Y") ?></div>
                                   <h4 class="ml-3"><?php the_title(); ?></h4>
                                </div>
                        </div>
                        <div class="col-12 contenidoArt">
                            <?php the_content(); ?>
                        </div>
                        <div class="container">
                            <div class="row pt-4 float-right">
                                <div class="col-8 col-sm-12">
                                    <button type="button" class="btn btn-primary"><i class="fab fa-facebook">  Compartir 3</i></button>
                                    <button type="button" class="btn btn-info"><i class="fab fa-twitter-square">  Compartir 5</i></button>
                                    <button type="button" class="btn btn-warning"><i class="fab fa-snapchat-square text-white"> Compartir 1</i></button>
                                </div>
                            </div>
                        </div>
                        <div class="container d-none d-md-block">
                            <div class="row my-5">

                                <div class="col-6 text-left"><?php previous_post_link("%link", "Leer Articulo Anterior "); ?> </div>
                                <div class="col-6 text-right"><?php previous_post_link("%link", "Leer Articulo Siguiente "); ?> </div>


                            </div>

                        </div>
                        <!-- slider articulos -->
                        <section class="jd-slider sliderArticulos pt-3">
                            <div class="slide-inner">
                                <ul class="slide-area ">
                                    <li class="px-2">
                                        <a href="articulos.html">
                                            <img src="img/articulo01.png" alt="arti1" class="img-fluid">

                                            <h6>Algun Titulo </h6>
                                        </a>
                                        <p class="small">
                                            Lorem ipsum dolor sit amet consectetur adipisicing el
                                        </p>
                                    </li>
                                    <li class="px-2">
                                        <a href="articulos.html">
                                            <img src="img/articulo02.png" alt="arti2" class="img-fluid">
                                            <h6>Algun Titulo </h6>
                                        </a>
                                        <p class="small">
                                            Lorem ipsum dolor sit amet consectetur adipisicing el
                                        </p>
                                    </li>
                                    <li class="px-2">
                                        <a href="articulos.html">
                                            <img src="img/articulo03.png" alt="arti3" class="img-fluid">

                                            <h6>Algun Titulo </h6>
                                        </a>
                                        <p class="small">
                                            Lorem ipsum dolor sit amet consectetur adipisicing el
                                        </p>
                                    </li>
                                    <li class="px-2">
                                        <a href="articulos.html">

                                            <img src="img/articulo04.png" alt="arti4" class="img-fluid">
                                            <h6>Algun Titulo </h6>
                                        </a>
                                        <p class="small">
                                            Lorem ipsum dolor sit amet consectetur adipisicing el
                                        </p>
                                    </li>

                                </ul>

                                <a class="prev" href="#">
                                    <i class="fa fa-angle-left text-muted"></i>
                                </a>
                                <a class="next" href="#">
                                    <i class="fa fa-angle-right text-muted"></i>
                                </a>

                            </div>
                            <div class="controller">
                                <div class="indicate-area">

                                </div>
                            </div>

                        </section>

                        <!-- fin slider -->
                        <div class="container">
                            <div class="row my-5">
                                <h4>Opiniones</h4>
                                <hr class="mb-3 mb-lg-5" style="border: 1px solid green; width: 100%">
                                <div class="d-flex">
                                    <div class="pr-2 w-50">
                                        <img src="img/user01.jpg" alt="user01" class="img-fluid">
                                    </div>
                                    <div class="flex-grow-1 textoopiniones">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore maxime alias nisi iure delectus, tenetur voluptatibus eum soluta, aut autem tempora voluptatem ducimus id, fuga tempore officia temporibus facere quam.</p>
                                        <span class="float-right">Plantilla Demo | 06-01-2018</span>
                                    </div>

                                </div>
                                <div class="d-flex text-right my-3">
                                    <div class="flex-grow-1 textoopiniones">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore maxime alias nisi iure delectus, tenetur voluptatibus eum soluta, aut autem tempora voluptatem ducimus id, fuga tempore officia temporibus facere quam.</p>
                                        <span class="float-right">Plantilla Demo | 06-01-2018</span>
                                    </div>
                                    <div class="pl-2 w-50">
                                        <img src="img/user02.jpg" alt="user02" class="img-fluid">
                                    </div>
                                </div>
                                <hr class="mb-3 mb-lg-5" style="border: 1px solid green; width: 100%">

                            </div>

                        </div>



<?php endwhile;
 endif;
  ?>  
</div>
<!--fin articulo -->
</div>
<!--fin contenido blog-->


<?php get_sidebar(); ?>


</div>
</div>
</div>


<?php get_footer(); ?>